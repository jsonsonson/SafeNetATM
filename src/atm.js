const fs = require('fs');

let stock;
const stockFile = process.env.STOCK || `${__dirname}/stock.json`;
const defaultStock = {
  100: 10,
  50: 10,
  20: 10,
  10: 10,
  5: 10,
  1: 10,
};
const red = '\x1b[31m';
const clear = '\x1b[0m';

/**
 * Clamp num within the range of a min and max
 *
 * @param  {Number} num Number to clamp
 * @param  {Number} min Range min
 * @param  {Number} max Range max
 * @return {Number}     Clamped number
 */
function clamp(num, min, max) {
  return Math.min(Math.max(num, min), max);
}

/**
 * Initialize the stock data file
 *
 * @return {undefined}
 */
function initStock() {
  try {
    // Create if doesn't exist
    if (!fs.existsSync(stockFile)) {
      fs.writeFileSync(stockFile, JSON.stringify(defaultStock, null, '  '));
    }

    if (stock === undefined) {
      stock = require(stockFile);
    }

    // Reinitialize bills with invalid values
    Object.keys(defaultStock).forEach(bill => {
      const billStock = parseInt(stock[bill]) || 0;

      if (billStock === undefined || billStock < 0) {
        stock[bill] = defaultStock[bill];
      }
    });

    // Remove unknown bills
    Object.keys(stock).forEach(bill => {
      if (defaultStock[bill] === undefined) {
        delete stock[bill];
      }
    });

    writeStock();
  } catch (err) {
    console.log(err);
    process.exit(-1);
  }
}

/**
 * Update the stock data file
 *
 * @return {undefined}
 */
function writeStock() {
  const stockStr = JSON.stringify(stock, null, '  ');
  fs.writeFileSync(stockFile, stockStr);
}

/**
 * Get the stock of desired denominations
 *
 * @param  {Array} denominations Bill denominations to retrieve stock for
 * @return {String}              String representation of the requested stock
 */
function getMachineBalance(denominations) {
  let balanceArr = [];

  if (denominations && denominations.length > 0) {
    denominations.forEach(denomination => {
      let bill = `${denomination}`;

      // Allow $ to preceed denominations
      if (bill.charAt(0) === '$') {
        bill = bill.substring(1, bill.length);
      }

      bill = parseInt(bill);
      const billStock = stock[`${bill}`];

      if (billStock !== undefined) {
        balanceArr.push(`$${bill} - ${billStock}`);
      }
    });
  }

  return balanceArr.join('\n');
}

/**
 * Reset stock back to the default stock
 *
 * @return {undefined}
 */
function restock() {
  // Clone defaultStock to avoid sharing references
  stock = JSON.parse(JSON.stringify(defaultStock));
  writeStock();

  console.log('\nMachine Balance:');
  console.log(`${getMachineBalance(Object.keys(defaultStock))}\n`);
}

/**
 * Withdraw an amount from the ATM
 *
 * @param  {Number} amount Amount to withdraw
 * @return {Boolean}       True if the transaction were successful, false otherwise
 */
function withdraw(amount) {
  // Replace any quotes if passed in with quotes
  const amountStr = `${amount}`.replace(/"/g, '');
  // Parse the amount or default to 0
  let amountLeft = parseInt(amountStr.substring(amountStr.charAt(0) === '$' ? 1 : 0, amountStr.length)) ||
    0;
  // This will keep track of the amount of bills to be used for each denomination
  const usedBills = {};

  if (amountLeft >= 0) {
    // Sort largest to smallest denomination so the largest bills are used first
    const bills = Object.keys(stock).sort((first, second) => {
      return parseInt(second) - parseInt(first);
    });

    for (let i = 0; i < bills.length; i++) {
      const bill = parseInt(bills[i]);
      const billStock = stock[`${bill}`];

      // Use the denomination if possible
      if (billStock > 0 && bill <= amountLeft) {
        // Determine how many bills can be used
        const maxNumBillsToUse = parseInt(amountLeft / bill);
        // Use only up to the amount of bills available
        const numBillsToUse = clamp(maxNumBillsToUse, 0, billStock);
        amountLeft -= numBillsToUse * bill;
        usedBills[`${bill}`] = numBillsToUse;
      }
    }
  } else {
    console.log(`${red}Failure: Cannot Dispense A Negative Amount${clear}`);
    return false;
  }

  // Successful transactions will have no amount left
  if (amountLeft === 0) {
    // Only apply the transaction on valid numbers
    if (!isNaN(`${`${amount}`.replace(/"/g, '')}`)) {
      // Decrease each used bills stock
      Object.keys(usedBills).forEach(bill => {
        stock[bill] = stock[bill] - usedBills[bill];
      });

      writeStock();

      console.log(`Success: Dispensed ${`${amount}`.charAt(0) !== '$' ? '$' : ''}${amount}\n`);
      console.log('Machine Balance:');
      console.log(`${getMachineBalance(Object.keys(defaultStock))}\n`);
      return true;
    }
  } else {
    console.log(`${red}Failure: Insufficient Funds${clear}`);
  }

  return false;
}

// Initialize the stock data file
initStock();

module.exports = {
  defaultStock,
  getMachineBalance,
  restock,
  withdraw,
};
