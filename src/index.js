#!/usr/bin/env node

const cli = require('wily-cli');
const {
  defaultStock,
  getMachineBalance,
  restock,
  withdraw,
} = require(`${__dirname}/atm.js`);

const red = '\x1b[31m';
const clear = '\x1b[0m';
const cyan = '\x1b[36m';

const restockCLI = cli
  .description('Restock the ATM to the default stock')
  .on('exec', () => {
    restock();
  });

const withdrawCLI = cli
  .description('Withdraw an amount from the ATM')
  .on('exec', (options, parameters) => {
    withdraw(parameters.amount);
  });

const denominationsCLI = cli
  .description('Display stock for denominations')
  .option('all', 'Display stock for all bill denominations')
  .on('exec', (options, parameters) => {
    // options.all will tell it to print all denominations
    const denominations = options.all ? Object.keys(defaultStock) : parameters.denominations || [];
    const balanceStr = getMachineBalance(denominations);

    if (balanceStr.length > 0) {
      console.log(`\n${balanceStr}\n`);
    }
  });

const argParser = cli.argParser();
argParser
  .version(`${require(`${__dirname}/../package.json`).version}`)
  .description('Manage a basic ATM')
  .command('R', 'Restock ATM', restockCLI)
  .command('W <amount>', 'Withdraws the amount from the ATM', withdrawCLI)
  .command(`I [denominations...]`, 'Displays the number of bills in that denomination present in the ATM', denominationsCLI)
  .on('exec', (options, parameters, command) => {
    if (command === undefined) {
      const argv0 = argParser.argv[0];

      // On invalid commands, or an invalid option without a command, print error
      if (argv0) {
        if (argv0.charAt(0) === (argParser.cli.options._switchChar || '-')) {
          console.log(`${red}Failure: Invalid Option ${argv0}. Try --help${clear}`);
        } else {
          console.log(`${red}Failure: Invalid Command ${argv0}. Try --help${clear}`);
        }
      }
    }
  })
  .enableInteractivity(`${cyan}atm$ `, 'Q', 'Quit the ATM application')
  .parse(['-h']); // Print the help output right away for immediate usage description
