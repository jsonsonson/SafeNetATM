const chai = require('chai');
const fs = require('fs');
const {
  defaultStock,
  getMachineBalance,
  restock,
  withdraw,
} = require('../../src/atm.js');

const oldLog = console.log;
const expect = (arg) => {
  console.log = oldLog;
  return chai.expect(arg);
};

describe('ATM', () => {
  beforeEach(() => {
    console.log = () => {}; // Disable log outputs of ATM - only see test output
    restock();
  });

  describe('getMachineBalance', () => {
    it('should return an array with the desired denomination stocks', () => {
      const denominations = ['1', '5', '$100'];
      const stocksStr = getMachineBalance(denominations);

      expect(stocksStr.includes('$1 -')).to.be.true;
      expect(stocksStr.includes('$5 -')).to.be.true;
      expect(stocksStr.includes('$100 -')).to.be.true;
    });

    it('should exclude non-desired denominations', () => {
      const denominations = ['1', '5', '$100'];
      const stocksStr = getMachineBalance(denominations);

      expect(stocksStr.includes('$10 -')).to.be.false;
      expect(stocksStr.includes('$20 -')).to.be.false;
      expect(stocksStr.includes('$50 -')).to.be.false;
    });

    it('should exclude non-existent denominations', () => {
      const denominations = ['1', '5', '$100', '22'];
      const stocksStr = getMachineBalance(denominations);

      expect(stocksStr.includes('$1 -')).to.be.true;
      expect(stocksStr.includes('$5 -')).to.be.true;
      expect(stocksStr.includes('$100 -')).to.be.true
      expect(stocksStr.includes('$22 -')).to.be.false;
    });
  });

  describe('restock', () => {
    it('should restock the ATM to the default stock value', () => {
      withdraw(10);
      restock();

      const stock = JSON.parse(fs.readFileSync(process.env.STOCK).toString());

      expect(stock).to.deep.equal(defaultStock);
    });
  });

  describe('withdraw', () => {
    context('with sufficient funds', () => {
      it('should allow the transaction and update the stock', () => {
        const result = withdraw(205);

        const stock = JSON.parse(fs.readFileSync(process.env.STOCK).toString());

        expect(result).to.be.true;
        expect(stock).to.not.deep.equal(defaultStock);
        expect(stock).to.deep.equal({
          100: 8,
          50: 10,
          20: 10,
          10: 10,
          5: 9,
          1: 10,
        });
      });

      it('should use the largest available bills', () => {
        withdraw(1000); // Remove all $100 bills
        withdraw(105);  // Use two $50 bills and a single $5 bill

        const stock = JSON.parse(fs.readFileSync(process.env.STOCK).toString());

        expect(stock).to.not.deep.equal(defaultStock);
        expect(stock).to.deep.equal({
          100: 0,
          50: 8,
          20: 10,
          10: 10,
          5: 9,
          1: 10,
        });
      });
    });

    context('with insufficient funds', () => {
      it('should not allow the transaction and keep the stock unmodified', () => {
        withdraw(208);              // Valid withdrawal
        withdraw(9);                // Valid withdrawal
        const result = withdraw(9); // Insufficient funds here

        const stock = JSON.parse(fs.readFileSync(process.env.STOCK).toString());

        expect(result).to.be.false;
        expect(stock).to.deep.equal({
          100: 8,
          50: 10,
          20: 10,
          10: 10,
          5: 8,
          1: 3,
        });
      });
    });

    context('with a negative withdrawal', () => {
      it('should not allow the transaction and keep the stock unmodified', () => {
        const result = withdraw(-10);

        const stock = JSON.parse(fs.readFileSync(process.env.STOCK).toString());

        expect(result).to.be.false;
        expect(stock).to.deep.equal(defaultStock);
      });
    });
  });
});
