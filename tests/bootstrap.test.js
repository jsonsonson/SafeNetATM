const fs = require('fs');

process.env.STOCK = `${__dirname}/test_stock.json`;

before((done) => {
  done();
});

after((done) => {
  fs.unlinkSync(process.env.STOCK);
  done();
});
