# [SafeNet ATM](https://gitlab.com/jsonsonson/SafeNetATM.git)

## Getting Started
This is a solution to the ATM problem. It's a CLI application that utilizes [wily-cli](https://www.npmjs.com/package/wily-cli), which is a command parsing/CLI creator tool that I wrote and published to NPM.

## Prerequisites
- [Node.js 8+](https://nodejs.org/en/)

## Installation
Run `npm link` from within the project's root directory. This will install all development and production dependencies, as well as set up the `atm` CLI tool.

## Running
After successful installation, all you need to do is run `atm` from within a terminal, and it'll start the application for you. You may also run `npm start` or `node src/index.js` from within the project's root directory. Inventory is persisted within `src/stock.json`.

## Testing
Run `npm t` from within the project's root directory. Tests, located in the `tests` directory, are written using the [Mocha](https://mochajs.org/) testing framework flavored with [Chai](http://www.chaijs.com/). Reports, including Mocha unit test reports and [nyc](https://www.npmjs.com/package/nyc) coverage report, are generated and written to the `reports` folder.

To ensure code style consistency, [eslint](https://www.npmjs.com/package/eslint) is set up for the source files. Run `npm run lint` from within the project's root directory to lint the project.

## Continuous Integration
Each push is ran against a CI setup for GitLab, which includes linting and testing, failing the build if either fails.

## CLI
```bash
Manage a basic ATM

Usage:  [command] [options]

Commands:
  R                     Restock ATM
  W <amount>            Withdraws the amount from the ATM
  I [denominations...]  Displays the number of bills in that denomination present in the ATM
  Q                     Quit the ATM application

Options:
  -h, --help     Output usage information
  -v, --version  Output version
```

#### R
```bash
Restock the ATM to the default stock

Usage:  R [options]

Options:
  -h, --help  Output usage information
```

#### W
```bash
Withdraw an amount from the ATM

Usage:  W <amount> [options]

Options:
  -h, --help  Output usage information
```

#### I
```bash
Display stock for denominations

Usage:  I [denominations...] [options]

Options:
  -h, --help  Output usage information
  -a, --all   Display stock for all bill denominations
```
